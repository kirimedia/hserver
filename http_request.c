#include <ctype.h>
#include <stdbool.h>
#include <string.h>

#include "http.h"
#include "http_request.h"

#define BUFFER_SIZE 4096

static int readline(http_read_t read, void *data, char *buffer, size_t buffer_size) {
	size_t i = 0;
	while (i < BUFFER_SIZE) {
		char c;
		int r = read(data, &c, sizeof(c));
		if (r <= 0)
			return -1;
		if (c == '\n')
			break;
		buffer[i++] = c;
	}

	if (i > 0 && buffer[i - 1] == '\r')
		i--;

	buffer[i] = '\0';

	return i;
}

static http_request_t *http_request_create() {
	return calloc(1, sizeof(http_request_t));
}

void http_request_destroy(http_request_t *request) {
	free(request->method);
	free(request->url);
	free(request->protocol);
	free(request->host);
	free(request);
}

http_request_t *http_request_parse(http_read_t read, void *data) {

	http_request_t *request = http_request_create();

	char buffer[BUFFER_SIZE];
	int buffer_len = readline(read, data, buffer, sizeof(buffer));
	if (buffer_len == -1)
		goto error;

	char *method_begin = buffer;
	char *method_end = strchr(method_begin, ' ');
	if (!method_end)
		goto error;

	request->method = strndup(method_begin, method_end - method_begin);

	char *url_begin = method_end + 1;
	while (isspace(*url_begin))
		url_begin++;
	char *url_end = strchr(url_begin, ' ');
	if (!url_end)
		goto error;

	request->url = strndup(url_begin, url_end - url_begin);

	char *protocol_begin = url_end + 1;
	while (isspace(*protocol_begin))
		protocol_begin++;
	char *protocol_end = buffer + buffer_len;

	request->protocol = strndup(protocol_begin, protocol_end - protocol_begin);

	while (true) {
		char buffer[BUFFER_SIZE];
		int buffer_len = readline(read, data, buffer, sizeof(buffer));
		if (buffer_len == -1)
			goto error;
		if (buffer_len == 0)
			break;

		char *header_name_begin = buffer;
		char *header_name_end = strchr(header_name_begin, ':');
		if (!header_name_end)
			continue;

		char *header_value_begin = header_name_end + 1;
		while (isspace(*header_value_begin))
			header_value_begin++;
		char *header_value_end = buffer + buffer_len;

		if (strncmp(header_name_begin, "Host", header_name_end - header_name_begin) == 0)
			request->host = strndup(header_value_begin, header_value_end - header_value_begin);
	}

	return request;

error:
	http_request_destroy(request);
	return NULL;
}
