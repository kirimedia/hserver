#ifndef HTTP_H
#define HTTP_H

#include <stdlib.h>

typedef int (*http_read_t)(void *data, char *buffer, size_t buffer_size);
typedef int (*http_write_t)(void *data,  char *buffer, size_t buffer_size);

int http_handle(const char *root, http_read_t read, http_write_t write, void *data);

#endif
