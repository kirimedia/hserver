client -------> server
	request
	response
	<------
	
	http

1.0 1.1
METHOD URL PROTOCOL\n
GET / HTTP/1.0\r\n
NAME: VALUE\r\n
NAME2: VALUE2\r\n
NAME3: VALUE3\r\n
Host: mail.ru\r\n
\r\n

PROTOCOL STATUS MESSAGE
HTTP/1.0 200 OK\r\n
NAME: VALUE\r\n
Content-Type: text/html image/jpg image/gif\r\n
Content-Length: LEN\r\n
\r\n
DATA

len(DATA) == LEN


HOST, URL
ROOT = /var/www/

ROOT + HOST + URL -> file
IF URL == '/' -> URL = /index.html
HOST -> localhost

GET / HTTP/1.0
HOST: mail.ru

/var/www/mail.ru/index.html

modules
1. http
	request_parse
	send_response
2. network
	open
	accept
	close
3. process
	daemonize
		hserver (pid: hserver_pid, ppid: bash_pid, stdin,stdout, stderr)
		close(stdin), close(stdout) close(stderr)
		fork -> hserver_child(pid: hserver_child_pid, ppid: hserver_pid)
		exit hserver -> hserver_child(pid: hserver_child-pid, ppid: 1 (init_pid))
		setsid();
	setup_signals
		SIGTERM -> exit
4. hserver
	
