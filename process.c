#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

#include "process.h"

volatile int process_exited = 0;

static void sigterm_handler(int signum) {
	process_exited = 1;
}

static void sigchld_handler(int signum) {
	while (waitpid(0, NULL, WNOHANG) <= 0)
		break;
}

static void exit_handler(int status, void *pidfile) {
	unlink(pidfile);
}

int process_daemonize() {
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	pid_t pid = fork();
	if (pid == -1)
		return -1;

	if (pid != 0)
		_exit(0);

	setsid();

	return 0;
}

int process_create_pidfile(const char *pidfile) {
	int fd = open(pidfile, O_WRONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
	if (fd == -1)
		return -1;

	if (dprintf(fd, "%u", getpid()) == -1)
		return -1;

	close(fd);

	on_exit(exit_handler, (char*)pidfile);

	return 0;
}

int process_setup_signals() {
	struct sigaction action = {0};
	action.sa_handler = sigterm_handler;
	if (sigaction(SIGTERM, &action, NULL) == -1)
		return -1;
	action.sa_handler = sigchld_handler;
	if (sigaction(SIGCHLD, &action, NULL) == -1)
		return -1;
	return 0;
}
