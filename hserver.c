#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "http.h"
#include "network.h"
#include "process.h"

typedef struct {
	char *root;
	char *address;
	uint16_t port;
	int backlog;
	char *pidfile;
} hserver_config_t;

int hserver_run(hserver_config_t *config) {

	if (process_daemonize() == -1)
		return -1;

	if (process_create_pidfile(config->pidfile) == -1)
		return -1;

	if (process_setup_signals() == -1)
		return -1;

	network_t *server = network_open(config->address, config->port, config->backlog);

	while (true) {
		network_t *client = network_accept(server);

		if (process_exited)
			break;

		if (!client)
			continue;

		pid_t child = fork();
		if (child == 0) {
			http_handle(config->root, network_read, network_write, client);
			network_close(client);
			_exit(0);
		}
	}

	network_close(server);

	return 0;
}

int main(int argc, char *argv[]) {

	hserver_config_t config = {
		.root = "/var/www/",
		.address = "127.0.0.1",
		.port = 80,
		.backlog = 128,
		.pidfile = "/var/run/hserver/hserver.pid",
	};

	return hserver_run(&config);
}
